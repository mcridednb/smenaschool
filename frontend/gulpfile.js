var gulp = require('gulp');
var jade = require('gulp-jade');
var stylus = require('gulp-stylus');


gulp.task('jade', function () {
     gulp.src(['./markup/pages/*.jade'])
        .pipe(jade())
        .pipe(gulp.dest('../build/'));
});

gulp.task('stylus', function () {
    return gulp.src('./*.styl')
        .pipe(stylus())
        .pipe(gulp.dest('./css'));
});

gulp.task('build', [
    'jade',
    'stylus'
]);